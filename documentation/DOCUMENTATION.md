## Documentation

**Documentation pending. Until then see examples/ for simple demo**

---

### Consumer

#### Metodes

**Consumer.constructor(ConsumerConfig,logger)**

**async connect()**

**async disconnect()**

**async subscribe([TopicConfig,TopicConfig,...])**

**async unsubscribe([Topic,Topic,....])**

**async unsubscribe()**

**async commit(Topic,Partition,Offset)**

**async commit()**

**async seek(Topic, Partition, Offset)**

**isConnected()**

**isSubscribed()**

**isAssigned()**

**isLoaded()**

#### Events

**consumer.on("connected",callback())**

**consumer.on("disconnected",callback())**

**consumer.on("subscribed",(callback())**

**consumer.on("unsubscribed",(callback())**

**consumer.on("assigned",callback([Assignment,Assignment,...]))**

**consumer.on("unassigned",callback([Assignment,Assignment,...]))**

**consumer.on("consuming",callback())**

**consumer.on("loaded",callback())**

#### Types

**ConsumerConfig**

**Logger**

**TopicConfig**

**DecoderType**

**TopicConsumeMode**

**Assignment**

**Topic**

**Partition**

**Offset**

---

### Producer 

**(AVRO functionality is not implemented yet. Need more testing)**

#### Metodes

**Producer.constructor(ProducerConfig,Logger)**

**async connect()**

**async disconnect()**

**async send(Topic, Message, Key, DeliverCallback, MessageType, Schema): TrackingToken**

**isConnected()**

### Events

**consumer.on("connected",()=>{})**

**consumer.on("disconnected",()=>{})**

**consumer.on("message.delivered",callback(TrackingToken))**

**consumer.on("queue.empty",callback())** (Not implemented)

**consumer.on("queue.full",callback())** (Not implemented)

#### Types

**ProducerConfig**

**Logger**

**MessageType**

**Schema**

**TrackingToken**

---

### Admin module (TO-DO)