enum DecoderTypeEnum {
    AUTO = 1,
    JSON = 2,
    AVRO = 3,
    STRING = 4,
    BUFFER = 5,
    CUSTOM = 6
}

enum ConsumeMode {
    EARLIEST = 1,
    LAST_COMMIT = 2,
    LATEST = 3,
    NEXT = 4
}

export class Consumer {

    static DecoderType: Readonly<DecoderTypeEnum>;
    static ConsumeMode: Readonly<ConsumeModeEnum>;

    constructor(consumerConfig: {
        groupId: string,
        clienId: string,
        maxQueueSize: bigint,
        maxFetchSize: bigint,
        maxWaitMs: bigint,
        statsIntervalSec: bigint,
        commitIntervalMs: boolean,
        schemaRegistryUrls: string[],
        kafkaBrokers: string[]
    }, logger:any);
    connect(): Promise<void>;
    disconnect(): Promise<void>;
    subscribe(topicConfigs: {
        topic: string,
        decoderType: DecoderTypeEnum,
        consumeMode: ConsumeModeEnum,
        autoCommit: boolean,
        keepOffsetOnReassign: boolean,
        decoder: (message: object) => Promise<object>,
        filter: (message: object) => Promise<boolean>,
        mapper: (message: object) => Promise<object>,
        processor: (message: object) => Promise<object>,
    }): Promise<void>;
    unsubscribe(topics?: string[]): Promise<void>;
    seek(topic: string, partition: bigint, offset: bigint): Promise<void>;
    commit(topic?: string, partition?: bigint, offset?: bigint): Promise<void>;
    isConnected(): boolean;
    isSubscribed(): boolean;
    isAssigned(): boolean;
    isLoaded(): boolean;
    on(eventName: "connected", callback: () => any): void;
    on(eventName: "disconnected", callback: () => any): void;
    on(eventName: "subscribed", callback: () => any): void;
    on(eventName: "unsubscribed", callback: () => any): void;
    on(eventName: "assigned", callback: () => any): void;
    on(eventName: "unassigned", callback: () => any): void;
    on(eventName: "consuming", callback: () => any): void;
    on(eventName: "loaded", callback: () => any): void;
    on(eventName: "stats", callback: (stats:object) => any): void;
    
}