"use strict";


//Use "PRINT_MESSAGES=[0,1] KAFKA_BROKERS=[ip]:[port] TOPIC=[demo topic] node producer-demo.js" to run


const clone = require("clone");
const Producer = require("../index.js").Producer;

async function main() {

    if (!process.env.TOPIC) {
        console.log('Use "PRINT_MESSAGES=[0,1] KAFKA_BROKERS=[ip]:[port] TOPIC=[demo topic] node producer-demo.js" to run');
        process.exit(0);
    }



    let schemaRegistryUrls = null;
    if (process.env.SCHEMA_REGISTRIES) schemaRegistryUrls = process.env.SCHEMA_REGISTRIES.split(",");
    //Setup producer
    const producer = new Producer(
        {
            clientId: "ProducerTest",
            kafkaBrokers: process.env.KAFKA_BROKERS.split(","),
            //schemaRegistryUrl: schemaRegistryUrls,
            maxMessagesInFlight: 500,
            minProduceLatencyMs: 10,
            statsIntervalSec: 1,
            compressionCodec: "none"
        }, console);

    //Connect consumer to broker
    await producer.connect();

    console.log("DEMO: Connected to broker");

    console.log("DEMO: Producing 100 messages to topic:");

    for (let counter = 0; counter < 100; counter++) {


        let sendToken = await producer.send(
            process.env.TOPIC,                                                      //Topic
            { id: counter, ts: (new Date()).toISOString(), msg: "Hello world!" },   //Message (value/null)
            counter,                                                                //Key (string/number/null)
            async (deliveryToken) => {                                              //Delivery callback. Make it async if blocking processing
                if (sendToken == deliveryToken) 
                    console.log(`Message delivered (SendToken: ${sendToken})`);
                else throw ("Token missmatch");
            },
            Producer.MessageType.JSON,                                              //Message type JSON/STRING/BUFFER/AVRO
            null                                                                    //Schema
        );
    }

    await producer.disconnect();

    console.log("DEMO DONE");
}

main();