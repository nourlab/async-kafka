"use strict";


//Use "PRINT_MESSAGES=[0,1] KAFKA_BROKERS=[ip]:[port] TOPIC=[demo topic] SCHEMA_REGISTRIES=[schema registry url] node consumer-demo.js" to run


const clone = require("clone");
const Consumer = require("../index.js").Consumer;

async function main() {

    if (!process.env.TOPIC) {
        console.log('Use "PRINT_MESSAGES=[0,1] KAFKA_BROKERS=[ip]:[port] TOPIC=[demo topic] SCHEMA_REGISTRIES=[schema registry url] node consumer-demo.js" to run');
        process.exit(0);
    }

    let schemaRegistryUrls = null;
    if (process.env.SCHEMA_REGISTRIES) schemaRegistryUrls = process.env.SCHEMA_REGISTRIES.split(",");

    //Setup consumer
    const consumer = new Consumer({
        kafkaBrokers: process.env.KAFKA_BROKERS.split(","), //Array of kafka brokers
        schemaRegistryUrls: schemaRegistryUrls,             //Array of schema registry URLs
        groupId: "DemoConsumerGroup",                       //If not specified a random GUID wil be used and commits will not function
        clientId: "MyDemoConsumer",                         //If not specified a random GUID wil be used
        maxQueueSize: 1000,                                 //Maximum number of messages to keep in async queue
        statsIntervalSec: 2,                                //Interval between stats calculation
        maxFetchSize: 500,                                  //Maximum number of messages to fetch in a batch. Processing of batch is syncrounous and influences flow.
        commitIntervalMs: 100,                              //Interval to commit offsets if topic has 'autoCommit' set
        maxEventLoopBlockMs: 10,                            //Maximum time in ms that batch processing will be allowed to block event loop.
        maxWaitMs: 10,                                      //Maximum time to wait to fill a batch of messages. Low number keeps latency low at the cost of IO
        logStats: true                                      //Log statistics to provided logger
    }, console);

    //Connect consumer to broker
    await consumer.connect();

    console.log("DEMO: Connected to broker");

    console.log("DEMO: Subscribing to topic and witing to return until eof is reached");

    //Subscribe to topic and return when eof is reached. Omit 'await' to just subscribe.
    await consumer.subscribe([{
        topic: process.env.TOPIC,                           //Name of topic on broker
        decoderType: Consumer.DecoderType.AUTO,             //Can be of type AUTO, JSON, AVRO, STRING and BUFFER
        consumeMode: Consumer.TopicConsumeMode.LAST_COMMIT, //Available modes EARLIEST, LAST_COMMITED, LATEST and NEXT
        autoCommit: true,                                   //If TRUE topic will be commitet minimum by the intervall specified in consumer property 'commitIntervalMs' and to latest processed message on unassign
        keepOffsetOnReassign: true,                        //If TRUE consumer will continue from after last processed message if partition is reassigned even if not commited
        filter: async (message) => {
            //If filter returns TRUE the message is passed on to further processing
            return true;
        },
        mapper: async (message) => {
            //Cloning the message even tough this is not necessary
            const resultMessage = clone(message);
            //Add custom property to message
            resultMessage.myTimestamp = new Date();
            //Return message to be passed on to processor
            return resultMessage;
        },
        processor: async (message,kafkaHeader) => {
            //Do something final with the message
            if (Number(process.env.PRINT_MESSAGES))
                console.log(message);
            //No need to return anything, only commit could happen after this
        },
    }]);

    console.log("DEMO: Topic read to end of file, continuing to process for 10 seconds");

    //Disconect after 10 seconds
    setTimeout(() => { consumer.disconnect() }, 10000)
}

main();