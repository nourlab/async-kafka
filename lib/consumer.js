"use strict";

//Package dependencies
const EventEmitter = require('events');
const Kafka = require('node-rdkafka');
const async = require("async");
const { v4: uuid } = require('uuid');
//const { Topic } = require('./topic');
const PartitionState = require('./partitionState');

//Project dependencies
const MessageDecoder = new require("./decodeMessage.js");
const TopicState = new require("./topicState.js");
const Logger = require("./logger.js");

class KafkaConsumer extends EventEmitter {

    // Static properties (ENUMS/Types)
    static ConsumerStatus = Object.freeze({ "DISCONNECTED": 1, "CONNECTED": 2, "SUBSCRIBED": 3, "ASSIGNED": 4, "LOADED": 6 })
    static TopicStatus = TopicState.TopicStatus;
    static TopicConsumeMode = TopicState.TopicConsumeMode;
    static TopicCommitMode = TopicState.TopicCommitMode;
    static PartitionStatus = TopicState.PartitionStatus;
    static DecoderType = MessageDecoder.DecoderType

    //Private properties
    #consumerConfiguration;
    #nodeRDKafkaConfig;
    #nodeRDKafkaTopicConfig;
    #decodeQueue;
    #topics = new Map();
    #status = KafkaConsumer.ConsumerStatus.DISCONNECTED;
    #decoder;
    #messageInFlight = false;
    #nodeRdKafkaConsumer;

    #statsProcessedCount = 0;
    #statsProcessedBytes = 0;
    #statsProcessedTotalLatency = 0;
    #statsMinimumBatchSize = null;
    #statsMaximumBatchSize = 0;
    #statsBatchTotalSize = 0;
    #statsProcessingYieldCount = 0;
    #statsBatchCount = 0;
    #statsQueueFullCount = 0;
    #statsTimestamp = new Date();
    #statsInterval = null;

    #disconnecting = false;
    #unassigning = false;
    #forcedDisconnecting = false;
    #logger;
    #inErrorShutdown = false;
    #batchTimestamp = null;
    #autoAdjustedBatchSize = null;
    #hasYielded = false;
    #processingCounter = 0;
    #yieldBatchSizes = [];


    //Class constructor
    constructor(consumerConfig, logger) {
        super();

        //Setup logger
        this.#logger = new Logger(logger);

        this.#logger.info("Initializing consumer");

        //Apply default values and store consumer config
        this.#consumerConfiguration = {
            groupId: consumerConfig.groupId || uuid(),
            clientId: consumerConfig.clientId || uuid(),
            maxQueueSize: consumerConfig.maxQueueSize || 2000,
            maxWaitMs: consumerConfig.maxWaitMs || 10,
            statsIntervalSec: consumerConfig.statsIntervalSec || null,
            commitIntervalMs: consumerConfig.commitIntervalMs || 100,
            schemaRegistryUrls: consumerConfig.schemaRegistryUrl || null,
            kafkaBrokers: consumerConfig.kafkaBrokers || ["localhost:9092"],
            logStats: consumerConfig.logStats || false,
            automaticGracefulShudown: consumerConfig.automaticGracefulShudown || true
        }

        if (consumerConfig.maxEventLoopBlockMs) this.#consumerConfiguration.maxEventLoopBlockMs = consumerConfig.maxEventLoopBlockMs;
        else this.#consumerConfiguration.maxEventLoopBlockMs = Math.round(this.#consumerConfiguration.maxWaitMs * 0.9);

        if (consumerConfig.maxFetchSize) this.#consumerConfiguration.maxFetchSize = consumerConfig.maxFetchSize;
        else this.#consumerConfiguration.maxFetchSize = this.#consumerConfiguration.maxQueueSize;

        //Send startup info to logger
        this.#logger.info("Using groupId: " + this.#consumerConfiguration.groupId);
        this.#logger.info("Using clientId: " + this.#consumerConfiguration.clientId);
        this.#logger.info("Using kafka brokers: " + JSON.stringify(this.#consumerConfiguration.kafkaBrokers));
        this.#logger.info("Using schema registers: " + JSON.stringify(this.#consumerConfiguration.schemaRegistryUrls));

        //Setup node-rdkafka consumer
        this.#nodeRDKafkaConfig = {
            'group.id': this.#consumerConfiguration.groupId,
            'metadata.broker.list': this.#consumerConfiguration.kafkaBrokers,
            "socket.keepalive.enable": true,
            "enable.auto.commit": true,
            "auto.commit.interval.ms": this.#consumerConfiguration.commitIntervalMs,
            "enable.partition.eof": true,
            "enable.auto.offset.store": false,
            "fetch.wait.max.ms": this.#consumerConfiguration.maxWaitMs,
            'rebalance_cb': (err, assignments) => {
                this.#onRebalance(err, assignments)
            },
            'offset_commit_cb': (err, topicPartitions) => {
                this.#onCommit(err, topicPartitions);
            }
        }
        this.#nodeRDKafkaTopicConfig = {
            "auto.offset.reset": "beginning",
        }
        this.#nodeRdKafkaConsumer = new Kafka.KafkaConsumer(this.#nodeRDKafkaConfig, this.#nodeRDKafkaTopicConfig);

        //Setup stats properties
        this.#statsMinimumBatchSize = this.#consumerConfiguration.maxQueueSize + 1;

        //Setup autotrottle properties
        this.#autoAdjustedBatchSize = this.#consumerConfiguration.maxFetchSize;

        //Capure eof event to detect when topics are loaded to current
        const self = this;
        this.#nodeRdKafkaConsumer.on('partition.eof', (eof_event) => { self.#processEOF(eof_event.topic, eof_event.partition, eof_event.offset) });

        //Setup messageDecoder
        this.#decoder = new MessageDecoder(this.#consumerConfiguration.schemaRegistryUrls);

        //Setup message processing queue
        this.#decodeQueue = async.queue(async (message) => {
            await this.#decodeMessage(message);
        }, 1);

        //Do gracefull shutdown on SIGINT, SIGTERM, uncaughtException and unhandledRejection if enabled (automaticGracefulShudown==true)
        if (this.#consumerConfiguration.automaticGracefulShudown) {
            process.on('SIGINT', () => {
                self.#logger.info(`Process ${process.pid} has been interrupted`);
                shutdownConsumer("external signal (SIGINT)");
            });

            process.on('SIGTERM', () => {
                self.#logger.info(`Process ${process.pid} received a SIGTERM signal`);
                shutdownConsumer("external signal (SIGTERM)");
            });

            process.on('uncaughtException', err => {
                self.#logger.error(`Uncaught Exception: ${err.message}`);
                handleExeption("uncaught Exception");
            })

            process.on('unhandledRejection', (reason, promise) => {
                self.#logger.error('Unhandled rejection at ', promise, `reason: ${reason.message}`);
                handleExeption("unhandled Rejection");
            })

            function handleExeption(reason) {
                if (!self.#inErrorShutdown) {
                    self.#inErrorShutdown = true;
                    shutdownConsumer(reason);
                } else {
                    self.#logger.error("Gracefull shutdown failed");
                }
            }

            async function shutdownConsumer(reason) {
                if (!self.#disconnecting && self.isConnected()) {
                    self.#logger.info("Starting gracefull shutdown of consumer in response to " + reason);
                    await self.disconnect();
                    self.#forcedDisconnecting = true;
                    self.#inErrorShutdown = false;
                    self.#logger.info("Gracefull shutdown of consumer complete");
                }
            }
        }
    }

    //Returns true if status of consumer is not DISCONNECTED
    isConnected() { return (this.#status != KafkaConsumer.ConsumerStatus.DISCONNECTED) };

    //Returns true if status of consumer is SUBSCRIBED, ASSIGNED or LOADED
    isSubscribed() { return (this.#status == KafkaConsumer.ConsumerStatus.SUBSCRIBED) || (this.#status == KafkaConsumer.ConsumerStatus.ASSIGNED) || (this.#status == KafkaConsumer.ConsumerStatus.LOADED) };

    //Returns true if status of consumer is ASSIGNED or LOADED
    isAssigned() { return (this.#status == KafkaConsumer.ConsumerStatus.ASSIGNED) || (this.#status == KafkaConsumer.ConsumerStatus.LOADED) };

    //Returns true if status of consumer is LOADED
    isLoaded() { return (this.#status == KafkaConsumer.ConsumerStatus.LOADED) };


    //Connects to kafka brokers and resolves when confirmed connection
    async connect() {
        //Only connect if unconnected
        if (!this.isConnected()) {
            this.#logger.info("Connecting consumer to broker(s): " + JSON.stringify(this.#consumerConfiguration.kafkaBrokers));
            await new Promise((resolve, reject) => {
                this.#nodeRdKafkaConsumer.connect(null, (err) => {
                    if (err) reject(err);
                    else resolve();
                });
            });
            //Start stats
            if (this.#consumerConfiguration.statsIntervalSec) this.#statsInterval = setInterval(() => this.#doStats(), this.#consumerConfiguration.statsIntervalSec * 1000);

            this.#setStatus(KafkaConsumer.ConsumerStatus.CONNECTED);
            this.emit("connected");
            this.#logger.info("Connected consumer to broker(s): " + JSON.stringify(this.#consumerConfiguration.kafkaBrokers));

            //Start background consuming of messages at low trottle
            this.#consumeMessages()
        } else throw ("Trying to connect a connected consumer")
    }

    //Disconnect from kafka broker(s). Will unsubscribe if subscribed. Resolves when confirmed unsubscribe and disconnect
    async disconnect() {
        if (!this.#forcedDisconnecting) {
            this.#logger.info("Disconnecting consumer from broker(s): " + JSON.stringify(this.#consumerConfiguration.kafkaBrokers));
            //Only disconnect if connected
            if (this.isConnected() && !this.#disconnecting) {
                this.#disconnecting = true;
                if (this.isSubscribed()) await this.unsubscribe();
                await new Promise(async (resolve, reject) => {
                    this.#nodeRdKafkaConsumer.disconnect((err) => {
                        if (err) reject(err);
                        else resolve();
                    });
                });

                this.#setStatus(KafkaConsumer.ConsumerStatus.DISCONNECTED);
                this.emit("disconnected")
                this.#logger.info("Disconnected consumer from broker(s): " + JSON.stringify(this.#consumerConfiguration.kafkaBrokers));

                //Stop stats
                if (this.#statsInterval) clearInterval(this.#statsInterval);

                this.#forcedDisconnecting = false;
                this.#disconnecting = false;
            } else throw ("Trying to disconnect a disconnected consumer");
        }
    }

    //Subscribe to new topics
    //Previous topics will be resubscribed and continue from where it left if autoCommit or keepOffsetOnReasign is enabled on topic.
    //Will resolve when all subscribed topics (including previous subscribtions) have reached end-of-file (consumer changes status to LOADED).
    async subscribe(topicConfigs) {
        //Subscribe if connected and not disconnecting
        if (this.isConnected() && !this.#disconnecting) {

            const subscriptions = [];

            //Get existing subscriptions
            this.#topics.forEach((topic) => {
                subscriptions.push(topic.getTopic());
            });

            //Setup and get new subscriptions
            const newSubscriptions = [];
            topicConfigs.forEach((topicConfig) => {
                if (!subscriptions.includes(topicConfig.topic)) {
                    //Create new topic state for topic
                    const topicState = new TopicState(topicConfig, this, this.#logger.getLogger());
                    this.#topics.set(topicConfig.topic, topicState);
                    subscriptions.push(topicConfig.topic);
                    newSubscriptions.push(topicConfig.topic);
                    //Subscribe to events from topic state
                    topicState.on("assigned", () => { this.#processTopicStatusEvent("assigned") });
                    topicState.on("unassigned", () => { this.#processTopicStatusEvent("unassigned") });
                    topicState.on("loaded", () => { this.#processTopicStatusEvent("loaded") });
                    topicState.on("seeking", () => { this.#processTopicStatusEvent("seeking") });
                }
                //Throw error if subscribtion exists
                else throw ("Topic is already subscribed: " + topicConfig.topic);
            });

            //Resubscribe to existing and new topics 
            if (subscriptions.length) {
                this.#logger.info("Subscribing to topics: " + JSON.stringify(newSubscriptions));

                //Call resubscribe with existing and new subscriptions
                await this.#reSubscribe(subscriptions);

                //Set consumer status to subscribed if consumer is not subscribed and there 
                if (!this.isSubscribed()) this.#setStatus(KafkaConsumer.ConsumerStatus.SUBSCRIBED);
            }
            //Throw error if no valid subscribsions for resubscribe
            else throw ("No valid subscriptions");

            //Resolve subscribe when all topics reach eof and consumer changes status to LOADED
            await new Promise((resolve, reject) => {
                this.once("loaded", () => {
                    this.#logger.info("Subscribed to topics: " + JSON.stringify(newSubscriptions));
                    resolve()
                });
            })

        }
        else throw ("Trying to subscribe on a disconnected consumer")
    };

    //Unsunscribe from existing topics if no topics are provided all topics wil be unsubscribed
    //Previous topics that are not unsubscribed will be resubscribed and continue from where it left if autoCommit or keepOffsetOnReasign is enabled on topic.
    //Will resolve when all unsubscribed topics are unnasigned.
    //???Sould probably be changed to resolve when all resubscribed topics have resumed from last processed offset 
    async unsubscribe(topics) {
        if (this.isSubscribed()) {
            const subscriptions = [];
            if (topics) {
                this.#logger.info("Unsubscribing from topics: " + JSON.stringify(topics));
                let existingTopics = true;
                topics.forEach((topic) => {
                    if (!this.#topics.has(topic)) existingTopics = false;
                })
                if (existingTopics) {
                    this.#topics.forEach((topic) => {
                        if (!topics.includes(topic.topic())) subscriptions.push(topic.topic());
                    });
                } else throw ("Trying to unsubscribe topics without subscrptions: " + JSON.stringify(topics));
            } else this.#logger.info("Unsubscribing from all topics");

            await this.#reSubscribe(subscriptions);
            if (!topics) this.#topics.clear();
            else topics.forEach((topic) => {
                this.#topics.delete(topic);
            });

            if ((this.#topics.size == 0) && this.isSubscribed()) this.#setStatus(KafkaConsumer.ConsumerStatus.CONNECTED);
            this.#logger.info("Unsubscribed from topic(s)");

        } else throw ("Trying to unsubscrobe from consumer without subscrptions")
    }

    //Commit a partition to give offset or commit all assigned topics if no topic and partition is provided
    //Resoves when commit is called on consumer
    //???Sould probably be changed to resolve when commit is confirmed by commit callback
    async commit(topic, partition, offset) {
        const commits = [];
        if (topic && (partition != null) && offset) {
            const partitionState = this.#getPartitionState(topic, partition);
            if (partitionState) {
                commits.push({ topic: topic, partition: partition, offset: offset });
                //this.#logger.info("Commit topics partition: " + topic + "/" + partition + " Offset: " + offset);
            } else throw ("Trying to commit offset on non existing partition");
            //Commit() will commit all topics with autoCommit and partitions with manual commits to latest processed offset
        } else {
            this.#topics.forEach(topicState => {
                //if (topicState.isAutoCommit() && topicState.isAssigned()) {
                if (topicState.isAssigned()) {
                    topicState.getPartitions().forEach(partitionState => {
                        if ((partitionState.getCommitedOffset() < partitionState.getProcessedOffset())
                            && ((partitionState.getCommitedOffset() !== null) || topicState.isAutoCommit())) commits.push({
                                topic: partitionState.getTopic(),
                                partition: partitionState.getPartition(),
                                offset: partitionState.getProcessedOffset()
                            });
                    })
                }
            })
            //this.#logger.info("Commiting all auto commit topics");
        }

        //Store to offset store for autocommit or use consumer commit if unassigning
        if (commits.length) {
            if (this.#unassigning) this.#nodeRdKafkaConsumer.commit(commits);
            else this.#nodeRdKafkaConsumer.offsetsStore(commits);

            //commits.forEach(topic => this.#logger.info("Committing topics/partition: " + topic.topic + "/" + topic.partition + " to offset " + topic.offset));
        }

    };

    //Set seeks an offset in given topic partition
    async seek(topic, partition, offset) {

        const partitionState = this.#getPartitionState(topic, partition);
        const seekOffset = partitionState.getSeekOffset();

        //Only seek if no ongoing seek in topic partition
        if (!seekOffset && (seekOffset !== 0)) {

            //Remove messages for topic partition from processing queue 
            this.#decodeQueue.remove((data) => { return ((data.data.topic == topic) && (data.data.partition == partition)); });
            await this.#waitForMessageInFlightToProcess();

            //Set seekMode in accordance to seek direction if no seekMode is set previous (in reassign or getMessages )
            const recievedOffset = partitionState.getRecievedOffset();
            const seekMode = partitionState.getSeekMode();
            if (seekMode == PartitionState.SeekMode.NONE) {
                if (recievedOffset) {
                    if (recievedOffset <= offset) partitionState.setSeekMode(PartitionState.SeekMode.FORWARD);
                    else partitionState.setSeekMode(PartitionState.SeekMode.BACKWARD);
                } else partitionState.setSeekMode(PartitionState.SeekMode.FORWARD);
            }

            //Set seekMode and seek offset in partitionState
            partitionState.seek(seekMode, offset);


            await new Promise((resolve, reject) => {
                this.#logger.log("Seeking " + topic + "/" + partition + " (" + offset + ")");

                //Call seek on consumer resolve when recieving messages or loaded
                //???This part need a rework to resolve when eof or first message for topic partition is recieved after seek
                this.#nodeRdKafkaConsumer.seek({ topic: topic, partition, offset: offset }, 10000, (err) => {
                    function resolvePromise() {
                        partitionState.removeListener("consuming", resolvePromise);
                        partitionState.removeListener("loaded", resolvePromise);
                        this.#logger.log("Found " + topic + "/" + partition + " (" + offset + ")");
                        resolve();
                    }
                    if (err) reject(err);

                    partitionState.once("consuming", resolvePromise);
                    partitionState.once("loaded", resolvePromise);

                });
            });
        }
    };

    #doStats() {
        const now = new Date();
        const diff = (now - this.#statsTimestamp) / 1000;
        const processingSpeed = Math.round(this.#statsProcessedCount / diff);
        const latency = this.#statsProcessedTotalLatency / this.#statsProcessedCount;
        const processingVolumeKbs = (this.#statsProcessedBytes / diff) / 1024;

        const averageBatchSize = this.#statsBatchTotalSize / this.#statsBatchCount;
        const batchSpeed = this.#statsBatchCount / diff;
        const yieldSpeed = this.#statsProcessingYieldCount / diff;
        const queueFullSpeed = this.#statsQueueFullCount / diff;
        const minimumBatchSize = this.#statsMinimumBatchSize;
        const maximumBatchSize = this.#statsMaximumBatchSize;

        if (this.#consumerConfiguration.logStats) this.#logger.log("Processed: " + processingSpeed + " msg/s (" + processingVolumeKbs.toFixed(0) + " kb/s)  Latency: " + latency.toFixed(2)
            + " ms - Batch size: " + averageBatchSize.toFixed(0) + " (avg) / " + minimumBatchSize + " (min) / " + maximumBatchSize
            + " (max) - Batches: " + batchSpeed.toFixed(1) + " events/s - Queue full: " + queueFullSpeed.toFixed(1) + " events/s - Process yields: " + yieldSpeed.toFixed(1) + " events/s");

        this.#statsBatchTotalSize = 0;
        this.#statsBatchCount = 0;
        this.#statsQueueFullCount = 0;
        this.#statsProcessingYieldCount = 0;
        this.#statsMaximumBatchSize = 0;
        this.#statsMinimumBatchSize = this.#consumerConfiguration.maxQueueSize + 1;
        this.#statsProcessedCount = 0;
        this.#statsProcessedBytes = 0;
        this.#statsProcessedTotalLatency = 0;
        this.#statsTimestamp = now;
    }

    #setStatus(status) {
        function propName(status) {
            for (var i in KafkaConsumer.ConsumerStatus) {
                if (KafkaConsumer.ConsumerStatus[i] == status) {
                    return i;
                }
            }
            return false;
        }
        if (status != this.#status) {
            this.#status = status;
            this.#logger.debug("CONNECTION " + propName(status) + ": Broker(s) " + JSON.stringify(this.#consumerConfiguration.kafkaBrokers) + " with clientID " + this.#consumerConfiguration.clientId);
        }
    }

    #isLastProcessedCommited(topic) {
        if (topic) {
            const topicState = this.#getTopicState(topic);
            return (topicState.isCommited() || !topicState.isAutoCommit());
        } else {
            let commited = true;
            this.#topics.forEach((topicState) => {
                if (!this.#isLastProcessedCommited(topicState.getTopic()))
                    commited = false;
            })
            return commited;
        }
    }

    async #waitForCommited(topic) {
        if (topic) this.#logger.info("Waiting for topic commited to last processed: " + topic);
        else this.#logger.info("Waiting for all topics commited to last processed");
        await async.whilst(
            async () => {
                //this.#logger.log(this.#getPartitionState("STAGE.entity.pass.key",0).getCommitedOffset(), this.#getPartitionState("STAGE.entity.pass.key",0).getProcessedOffset() )
                return !this.#isLastProcessedCommited(topic);
            },
            async () => {
                await new Promise((resolve, reject) => {
                    setTimeout(() => resolve(), 1000);
                })
            });
        if (topic) this.#logger.info("Topic commited to last processed: " + topic);
        else this.#logger.info("All auto commit topics commited to last processed");
    }

    #getTopicState(topic) {
        let topicState = this.#topics.get(topic);
        if (!topicState) throw ("Trying to get unknown topicState: " + topic)
        else return topicState;
    }

    #getPartitionState(topic, partition) {
        const topicState = this.#getTopicState(topic);
        return topicState.getPartition(partition);
    }

    #processTopicStatusEvent(event) {
        if (event == "seeking") {
            if (this.isLoaded()) {
                this.#setStatus(KafkaConsumer.ConsumerStatus.ASSIGNED);
                //this.emit("subscribed");
            }
            else if (!this.isAssigned()) throw ("Topic send seeking event to unassigned consumer");
        }
        else if (event == "assigned") {
            if (!this.isAssigned() && this.isSubscribed()) {
                this.#setStatus(KafkaConsumer.ConsumerStatus.ASSIGNED);
                this.emit("assigned");
            }
            else if (!this.isSubscribed()) throw ("Topic send assign event to unsubscribed consumer");
        }
        else if (event == "loaded") {
            let loaded = true;
            this.#topics.forEach((topic) => {
                if (!topic.isLoaded() && topic.isAssigned()) loaded = false;
            });
            if (loaded && !this.isLoaded()) {
                this.#setStatus(KafkaConsumer.ConsumerStatus.LOADED);
                this.emit("loaded");
            }
            else if (!this.isAssigned()) throw ("Topic sent loaded event to unassigned consumer");
        }
        else if (event == "unassigned") {
            let assigned = false;
            this.#topics.forEach((topic) => {
                if (topic.isAssigned()) assigned = true;
            });
            if (!assigned && this.isAssigned()) {
                this.#setStatus(KafkaConsumer.ConsumerStatus.SUBSCRIBED);
                this.emit("unassigned");
            }
            else if (!this.isAssigned()) throw ("Topic send Unassign event to unassigned consumer");
        }
    }

    #processEOF(topic, partition, offset) {
        if (!this.#disconnecting && this.isAssigned()) {
            const partitionState = this.#getPartitionState(topic, partition);
            partitionState.recievedEof(offset);
        }
    }

    //Called on change in subscribtions. First unsunscribes from all topics then resubscribe to kept topics or subscribe to new topics
    async #reSubscribe(subscriptions) {

        const keepSubscriptions = [];
        if (this.isSubscribed()) {
            //Unsubscribe from all topics. (Only way in node-rdkafka, topics to keep will be resubscribed next)
            await new Promise((resolve, reject) => {
                //If consumer is assigned unsubscribe is resolved when consumerStatus changes to CONNECTED
                if (this.isAssigned()) {

                    this.#nodeRdKafkaConsumer.unsubscribe();
                    this.once("unassigned", () => {

                        resolve();
                    });
                } else {
                    this.#nodeRdKafkaConsumer.unsubscribe();
                    resolve()
                }
            });
        };
        if (this.isConnected()) {
            //Resubscribe or subscribe to new topics if any
            if (subscriptions.length) {
                this.#nodeRdKafkaConsumer.subscribe(subscriptions);
            }
        }
    }

    //Called on all rebalance events like subscribe, unsubscribe and rebalance
    #onRebalance(err, assignments) {
        if (err.code === Kafka.CODES.ERRORS.ERR__ASSIGN_PARTITIONS) {
            this.#logger.info("Assigning partitions");
            const seeks = [];
            //Process each assignment to set status
            assignments.forEach((assignment) => {

                const topicState = this.#getTopicState(assignment.topic);
                const partitionState = topicState.assignPartition(assignment.partition)
                const topicConsumeMode = topicState.getConsumeMode();

                //Identify TopicConsumeModes and possible keepOffsetOnReassign and set SeekMode and offsets accordingly
                //As higgwater offset is not awailable at this point TopicConsumeMode LATEST and NEXT will issue a seek with offset on first recieved message.
                //If reasign set start offset of assignment before activating
                if (partitionState.getSeekMode() == PartitionState.SeekMode.REASSIGN) {
                    if (!topicState.isAutoCommit()) {
                        assignment.offset = partitionState.getSeekOffset();
                        partitionState.seek(PartitionState.SeekMode.REASSIGN, null);
                    }
                }
                //If TopicConsumeMode is LAST_COMMIT set SeekMode of partition to COMMITED 
                else if (topicConsumeMode == TopicState.TopicConsumeMode.LAST_COMMIT) {
                    partitionState.seek(PartitionState.SeekMode.COMMITED, null);
                }
                //If TopicConsumeMode is EARLIEST set start offset to 0 of assignment before activating
                else if (topicConsumeMode == TopicState.TopicConsumeMode.EARLIEST) {
                    assignment.offset = 0;
                    partitionState.seek(PartitionState.SeekMode.EARLIEST, null);
                }
                //If TopicConsumeMode is LATEST set SeekMode of partition to LATEST 
                else if (topicConsumeMode == TopicState.TopicConsumeMode.LATEST) {
                    partitionState.seek(PartitionState.SeekMode.LATEST, null);
                }
                //If TopicConsumeMode is NEXT set SeekMode of partition to NEXT 
                else if (topicConsumeMode == TopicState.TopicConsumeMode.NEXT) {
                    partitionState.seek(PartitionState.SeekMode.NEXT, null);
                }

                if (this.isLoaded()) this.#setStatus(KafkaConsumer.ConsumerStatus.ASSIGNED);
            })

            // Call assign on consumer
            this.#nodeRdKafkaConsumer.assign(assignments);
            this.#logger.info("Partitions assigned");
        }
        // Handle partition unassign. 
        else if (err.code === Kafka.CODES.ERRORS.ERR__REVOKE_PARTITIONS) {
            this.#unassigning = true;
            this.#logger.info("Unassigning partitions");
            //Finnish prosessing of message in flight and clear processing queue
            this.#clearQueueAndEndProcessing().then(() => {
                //Commit all autoCommit topics
                this.commit();
                //Wait for all topics to be commited
                this.#waitForCommited().then(() => {
                    // Call unassign on consumer
                    this.#nodeRdKafkaConsumer.unassign();
                    //Process each assignment to set status
                    assignments.forEach((assignment) => {
                        const partitionState = this.#getPartitionState(assignment.topic, assignment.partition);
                        this.#logger.log("Stopped processing: " + partitionState.getTopic() + "/" + partitionState.getPartition() + " Offset: " + partitionState.getProcessedOffset());
                        partitionState.unassign();
                    });
                    this.#logger.info("Partitions unassigned");
                    this.#unassigning = false;
                });
            })

        } else {
            throw (err);
        }
    }

    async #consumeMessages() {
        //Slow read loop waiting for assignment
        await async.whilst(
            //Repeat slow loop while not disconnecting or dissconnected
            async () => {
                return (this.isConnected());
            },
            //While worker is assigned get batches of messages to fill decodeQueue
            async () => {
                //Fast read loop while assigned
                await async.doWhilst(
                    //doWhilst worker fetching a batch of messages if queue is not full
                    async () => {
                        await new Promise((resolve, reject) => {
                            //Calulate number of messages to get limited by maxQueueSize and maxFetchSize
                            const inQueues = this.#decodeQueue.length();
                            let messageCount = Math.round((this.#consumerConfiguration.maxQueueSize - inQueues) / 1);
                            //if (messageCount >= this.#consumerConfiguration.maxFetchSize) messageCount = this.#consumerConfiguration.maxFetchSize;
                            if (messageCount >= this.#autoAdjustedBatchSize) messageCount = this.#autoAdjustedBatchSize;
                            else messageCount = 0;

                            //Get messages if room in queue
                            if (messageCount) {
                                const self = this;
                                //Process batch of recieved messages into decodeQueue or start seek after reassign. Resolve enclosing promise when done
                                //???Could be extracted to an external async function for tidyness
                                function recieveMessages(err, messages) {
                                    //Set batch timestamp for auto trotling
                                    self.#batchTimestamp = (new Date());
                                    //Get local recieved timestamp
                                    const recievedTimestamp = new Date();
                                    if (((self.#status == KafkaConsumer.ConsumerStatus.ASSIGNED) || (self.#status == KafkaConsumer.ConsumerStatus.CONSUMING) || (self.#status == KafkaConsumer.ConsumerStatus.LOADED)) && !self.#unassigning) {
                                        if (err) reject(err);

                                        //Process each message 
                                        messages.forEach((message) => {
                                            const partitionState = self.#getPartitionState(message.topic, message.partition);
                                            const lastRecievedOffset = partitionState.getRecievedOffset();

                                            let skipMessage = false;

                                            //Handle partition that is in seek mode, starting seeks that require highwater offset and identifiy is seek objective is reached
                                            if (partitionState.isSeeking()) {

                                                const seekMode = partitionState.getSeekMode();
                                                const seekOffset = partitionState.getSeekOffset();
                                                const watermarkOffsets = self.#nodeRdKafkaConsumer.getWatermarkOffsets(message.topic, message.partition);
                                                //this.#logger.log(watermarkOffsets);

                                                //Seek to last message if seekMode LATEST 
                                                if ((seekMode == PartitionState.SeekMode.LATEST) && !seekOffset) {
                                                    //partitionState.setRecievedOffset(watermarkOffsets.highOffset-1);
                                                    skipMessage = true;
                                                    self.seek(message.topic, message.partition, watermarkOffsets.highOffset - 1);
                                                }
                                                //Set seek offset to next offset if SeekMode COMMITED and no seekofset set to skip first message that has been processed
                                                else if ((seekMode == PartitionState.SeekMode.COMMITED) && !seekOffset) {
                                                    skipMessage = true;
                                                    partitionState.seek(PartitionState.SeekMode.COMMITED, message.offset + 1);
                                                }
                                                //Seek to eof if seekMode NEXT 
                                                else if ((seekMode == PartitionState.SeekMode.NEXT) && !seekOffset) {
                                                    skipMessage = true;
                                                    self.seek(message.topic, message.partition, watermarkOffsets.highOffset);
                                                }
                                                //Detect if objective of a BACKWARD seek is not reached
                                                else if (seekMode == PartitionState.SeekMode.BACKWARD) {
                                                    if (message.offset > lastRecievedOffset) skipMessage = true;
                                                }
                                                //Detect if objective of a FORWARD seek is not reached
                                                else if (seekMode == PartitionState.SeekMode.FORWARD) {
                                                    if (message.offset < seekOffset) skipMessage = true;
                                                }
                                                //Detect if objective of a FORWARD seek is not reached
                                                else if (seekMode == PartitionState.SeekMode.LATEST) {
                                                    if (message.offset < seekOffset) skipMessage = true;
                                                }
                                                //Detect if objective of a NEXT seek is not reached
                                                else if (seekMode == PartitionState.SeekMode.NEXT) {
                                                    if (message.offset < seekOffset) skipMessage = true;
                                                }
                                                //else if (message.offset <= lastRecievedOffset) skipMessage = true;

                                            }
                                            //If not seeking message.offset should be larger than lastRecievedOffset. Warn and skip if not (This indicate an error in this consumer)
                                            else {
                                                if ((message.offset <= lastRecievedOffset) && (lastRecievedOffset !== null)) {
                                                    skipMessage = false;
                                                    self.#logger.error("Message out of order, skipping message and continueing: " + message.topic + "/" + message.partition + " (" + message.offset + "<=" + lastRecievedOffset + ")");
                                                }
                                            }

                                            //Queue message for decoding if not skipping
                                            if (!skipMessage) {
                                                //Send startup message for partition to logger
                                                if (!partitionState.getFirstRecievedOffset()) {
                                                    const seekOffset = partitionState.getSeekOffset();
                                                    if ((partitionState.getSeekMode() == PartitionState.SeekMode.FORWARD) || (partitionState.getSeekMode() == PartitionState.SeekMode.BACKWARD) || (seekOffset === 0)) this.#logger.log("Resuming processing after seek: " + message.topic + "/" + message.partition + " Offset: " + message.offset);
                                                    else if (lastRecievedOffset) self.#logger.log("Resuming processing after reassign: " + message.topic + "/" + message.partition + " Offset: " + message.offset);
                                                    else self.#logger.info("Starting processing: " + message.topic + "/" + message.partition + " Offset: " + message.offset);
                                                    self.emit("consuming");
                                                }
                                                //Add recieved timestamp to message
                                                message.recievedTimestamp = recievedTimestamp;
                                                //Update lastRecievedOffset in partition state
                                                partitionState.setRecievedOffset(message.offset);
                                                //Submit message to processing queue
                                                self.#decodeQueue.push(message);
                                            }
                                        })
                                    };

                                    //Update batch stats
                                    const batchSize = messages.length;
                                    if (batchSize < self.#statsMinimumBatchSize) self.#statsMinimumBatchSize = batchSize;
                                    if (batchSize > self.#statsMaximumBatchSize) self.#statsMaximumBatchSize = batchSize;
                                    self.#statsBatchTotalSize = self.#statsBatchTotalSize + batchSize;
                                    self.#statsBatchCount++;

                                    //Yeld and resolve fast loop (setImediate added to give time to other processes possibly fixing bug #5)
                                    setImmediate(() => { resolve(); })

                                };
                                //Get new batch of messages from consumer
                                this.#nodeRdKafkaConsumer.consume(messageCount, recieveMessages);
                            }
                            // Delay fast loop read retry if queue is full with maxWaitMs
                            else {
                                //Update queue stats
                                this.#statsQueueFullCount++;


                                //const waitTime=Math.round(this.#consumerConfiguration.maxWaitMs/2);
                                //if(!waitTime) waitTime=1;
                                setTimeout(() => {
                                    resolve();
                                }, this.#consumerConfiguration.maxWaitMs);
                            }
                        });
                    },
                    //Repeat fast read loop as long as consumer is assigned
                    async () => {
                        return (this.isAssigned());
                    }
                );
                // Trottle down if connected but not assigned
                await new Promise((resolve, reject) => {
                    setTimeout(() => {
                        resolve();
                    }, 1000);
                })
            }
        );
    }

    //Process commit callbacks from node-rdkafka consumer
    #onCommit(err, topicPartitions) {
        //Throw error if recieved from consumer
        if (err) throw (err);
        //Update commited offset for each partition commit recieved
        topicPartitions.forEach(topicPartition => {
            if (topicPartition.offset) {
                //Update last commited offset for partition
                const partitionState = this.#getPartitionState(topicPartition.topic, topicPartition.partition);
                partitionState.setCommitedOffset(topicPartition.offset);
            }
        })
    };

    //Private async function that will empty processing queue and wait for message beeing processed to finnish before resolving
    async #clearQueueAndEndProcessing() {
        //Remove all waiting entries in processing queue
        this.#decodeQueue.remove((data) => { return true; });
        //If message is beeing processed wait for processing to finnish
        await this.#waitForMessageInFlightToProcess();
        this.#logger.info("Queue emptied and no messages processing");
    }

    async #waitForMessageInFlightToProcess() {
        //If message is beeing processed wait for processing to finnish
        this.#logger.info("Check if message is processing and wait until finnished");

        await new Promise((resolve, reject) => {
            if (this.#messageInFlight) {
                console.log(this.#messageInFlight);
                this.#logger.debug("Message in flight");
                this.once("_messageProcessed", () => {
                    resolve();
                });
                const rejectTimeout = setTimeout(() => {
                    this.#logger.error("Waiting for processing of message to finnishe timed out");
                    reject("Waiting for processing of message to finnishe timed out");
                }, 5000);
            } else resolve();
        });
    }

    //Private async function that is activated by the processing queue in a single tread (one message at a time) to decode and process recieved messages.
    //Messages follows the path DECODE>FILTER>MAP>PROCESS>COMMIT where all stages after decode is optional
    async #decodeMessage(message) {
        if (!this.#disconnecting || this.#unassigning) {
            //Set messageInFlight flag to aid clean unassign of partitions in clearQueueAndEndProcessing
            this.#messageInFlight = true;


            //Get states
            const topicState = this.#getTopicState(message.topic);
            const partitionState = topicState.getPartition(message.partition);


            //Decode message
            const decoderResult = await this.#decoder.decodeMessage(message, topicState.getDecoderType(), topicState.getDecoder());

            //create a kafka message header state fo follow message through processing
            //???Some refacoring could make this obsolete and faster
            const kafkaHeader = {
                key: decoderResult.key,
                offset: decoderResult.offset,
                partition: decoderResult.partition,
                size: decoderResult.size,
                timestamp: decoderResult.timestamp,
                recievedTimestamp: message.recievedTimestamp,
                topic: decoderResult.topic,
                decoderType: decoderResult.decoderType,
                decodeMode: decoderResult.decodeMode
            }

            //???Some refacoring could make this obsolete and faster
            const decodedMessage = {
                kafkaHeader: kafkaHeader,
                value: decoderResult.value
            }

            //Call custom filter if defined
            let filterResult = true;
            const filter = topicState.getFilter();
            if (filter) filterResult = await filter(decodedMessage.value, decodedMessage.kafkaHeader);
            //Process filtered messages
            if (filterResult) {

                //Call custom mapper if defined
                const mapper = topicState.getMapper();
                if (mapper) decodedMessage.value = await mapper(decodedMessage.value, decodedMessage.kafkaHeader)

                //Call custom processor if defined
                const processor = topicState.getProcessor();
                if (processor) await processor(decodedMessage.value, decodedMessage.kafkaHeader)

                //Set processed offset of partition before commit when processing is done
                partitionState.setProcessedOffset(decodedMessage.kafkaHeader.offset);

                //Update stats
                this.#statsProcessedCount++;
                this.#statsProcessedBytes = this.#statsProcessedBytes + decodedMessage.kafkaHeader.size;
                const now = new Date();
                const diff = now - decodedMessage.kafkaHeader.recievedTimestamp;
                this.#statsProcessedTotalLatency = this.#statsProcessedTotalLatency + diff;


                //If autoCommit is enabled for topic store commit offset in consumer for commit during auto commit cycle skip if disconnecting as this will be handeled in unassign
                if (!this.#disconnecting)
                    if (topicState.isAutoCommit())
                        this.#nodeRdKafkaConsumer.offsetsStore([{ topic: message.topic, partition: message.partition, offset: message.offset }]);
            }


            //Yield to other tasks if blocking event loop to long
            this.#processingCounter++;
            let yieldProcessing = false;
            const now = new Date();
            const timeDiff = now - this.#batchTimestamp;
            if (timeDiff >= this.#consumerConfiguration.maxEventLoopBlockMs) {
                this.#batchTimestamp = now;
                if (!this.#hasYielded) {
                    this.#hasYielded = true;
                    this.#yieldBatchSizes.push(this.#processingCounter);
                    if (this.#yieldBatchSizes.length > 50) this.#yieldBatchSizes.shift();
                    //console.log("YIELD");
                    this.#statsProcessingYieldCount++;
                }
                if (!this.#disconnecting&&!this.#unassigning) yieldProcessing = true;

            }

            //Optimize batch length on end of batch 
            if (!this.#decodeQueue.length()) {
                if (this.#hasYielded) {
                    if (this.#yieldBatchSizes.length > 10) {
                        let newSize = this.#yieldBatchSizes.reduce((a, b) => a + b) / this.#yieldBatchSizes.length;
                        //if (newSize < 1) newSize = 1;
                        this.#autoAdjustedBatchSize = Math.round(newSize);
                    }
                    this.#hasYielded = false;
                }
                else {
                    let newSize;
                    if (this.#autoAdjustedBatchSize >= 100) newSize = this.#autoAdjustedBatchSize * 1.01;
                    else newSize = this.#autoAdjustedBatchSize + 1;
                    if (newSize > this.#consumerConfiguration.maxFetchSize) newSize = this.#consumerConfiguration.maxFetchSize;
                    this.#autoAdjustedBatchSize = Math.round(newSize);

                }
                this.#processingCounter = 0;
                //console.log(this.#autoAdjustedBatchSize);
            }

            //Clear in flight flag to aid clean unassignment of partitions
            this.#messageInFlight = false;
            this.emit("_messageProcessed");

            //Yield to event loop if blocking
            if (yieldProcessing) await new Promise((resolve, reject) => { setImmediate(() => resolve()) });
        };
    }

}

module.exports = KafkaConsumer;
