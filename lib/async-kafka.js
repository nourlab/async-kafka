"use strict";

const Consumer = require("./consumer.js");
const Producer = require("./producer.js");;
const Admin = null;

module.exports = {
    Consumer,
    Producer,
    Admin
};