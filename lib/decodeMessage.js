const sharedFunctions = require("./shared.js");
const clone = require("clone");

/*var registry = null;

var schemaRegistryUrl = null;



const MessageDecoderX = {
    decodeMessage: decodeMessage,
    schemaRegistryUrl: schemaRegistryUrl
}*/

class MessageDecoder {

    static DecoderType = Object.freeze({ "AUTO": 1, "JSON": 2, "AVRO": 3, "STRING": 4, "BUFFER": 5, "CUSTOM": 6 })

    constructor(schemaRegistryUrl) {
        this.schemaRegistryUrl = schemaRegistryUrl;
        if (schemaRegistryUrl) {
            const AvroCoder = new require("./avroCoder.js");
            this.registry = new AvroCoder(schemaRegistryUrl);
        }
        //this.registry = require('avro-schema-registry')(schemaRegistryUrl);
    }

    //!!!This function is has side-effects changing the input object data to avoid unnessesary cloning giving lower performance
    async decodeMessage(data, decoderType,customDecoder) {
        //console.log(JSON.stringify(data.value))

        const result = await new Promise((resolve, reject) => {
            let registry = this.registry;
            let key = null;
            if (data.key) key = data.key.toString();

            const decodedData = {
                key: key,
                offset: data.offset,
                partition: data.partition,
                size: data.size,
                timestamp: new Date(data.timestamp),
                recieveTimestamp: data.recieveTimestamp,
                topic: data.topic,
                decoderType: decoderType,
                autoDecode:null,
                value: null
            }

            if (data.value.length != 0) {
                if (decoderType == MessageDecoder.DecoderType.AUTO) {
                    decodedData.autoDecode = true;
                    //if (data.value.length != null) {
                    if (data.value.readUInt8(0) !== 0) {
                        let stringValue = data.value.toString();
                        // Convert string to javascript object if JSON
                        decodedData.value = sharedFunctions.convertIfJSON(stringValue);
                        if ((typeof decodedData.value === 'object') && (decodedData.value !== null)) decodedData.decodedType =MessageDecoder.DecoderType.JSON;
                        else decodedData.decodedType = MessageDecoder.DecoderType.STRING;
                        resolve(decodedData);
                    } else {
                        if (registry) {
                            // Decode Avro message
                            registry.decode(data.value).then(result => {
                                decodedData.value = result;
                                decodedData.decodedType = MessageDecoder.DecoderType.AVRO;
                                resolve(decodedData);
                            }).catch(err => {
                                reject(err);
                            });

                        }
                        else reject(new Error("No registry set up"));
                    }
                    //}
                } else {
                    decodedData.autoDecode = false;
                    if (decoderType == MessageDecoder.DecoderType.JSON) {
                        try {
                            let stringValue = data.value.toString();
                            // Convert string to javascript object if JSON
                            decodedData.value = sharedFunctions.convertIfJSON(stringValue);
                            decodedData.decodedType = MessageDecoder.DecoderType.JSON;
                            resolve(decodedData);
                        } catch (e) {
                            reject(new Error("Massage is not in JSON."));
                        }
                    } else if (decoderType == MessageDecoder.DecoderType.AVRO) {
                        //console.log(JSON.stringify(data.value))
                        registry.decode(data.value).then(result => {
                            decodedData.value = result;
                            decodedData.decodedType = MessageDecoder.DecoderType.AVRO;
                            resolve(decodedData);
                        }).catch(err => {
                            reject(err);
                        });
                    } else if (decoderType == MessageDecoder.DecoderType.STRING) {
                        decodedData.value = data.value.toString();
                        decodedData.decodedType = MessageDecoder.DecoderType.STRING;
                        resolve(decodedData);
                    } else if (decoderType == MessageDecoder.DecoderType.BUFFER) {
                        //data.value = data.value;
                        decodedData.value = clone(data.value);
                        decodedData.decodedType = MessageDecoder.DecoderType.BUFFER;
                        resolve(decodedData);
                    } else if (decoderType == MessageDecoder.DecoderType.CUSTOM) {
                        if(customDecoder){
                            customDecoder(data.topic,data.value).then(result => {
                                decodedData.value = result;
                                decodedData.decodedType = MessageDecoder.DecoderType.CUSTOM;
                                resolve(decodedData);
                            }).catch(err => {
                                reject(err);
                            });
                        } else reject(new Error("Custom decoder is not provided"));
                    }else reject(new Error("Unknown DecoderType. (custom decoder is not implemented"));
                }
            } else resolve(decodedData);
        });
        return result;
    }
}

module.exports = MessageDecoder;