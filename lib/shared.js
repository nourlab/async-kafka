var sharedLib = {};

module.exports = sharedLib;

sharedLib.convertIfJSON = _convertIfJSON;
sharedLib.now = now;
sharedLib.getLogger = _getLogger;
sharedLib.isOlderThanMs = _isOlderThan;

const env = process.env.NODE_ENV || 'development';


function _isOlderThan(date, age) {
  const now = new Date();
  const then = new Date(date);
  const diff = now - date;
  if (diff > age) return true;
  else return false;
}

function _getLogger(externalLogger, moduleName) {
  var logger;
  if (!externalLogger) {
    const winston = require("winston");
    const logLevel = process.env.LOG_LEVEL || 'info';
    var prefix = "";
    if (moduleName) prefix = moduleName + ": ";
    logger = winston.createLogger({
      level: logLevel,

      format: winston.format.combine(
        winston.format.colorize(),
        //winston.format.simple(),
        winston.format.timestamp({
          format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.label({ label: moduleName }),

        //winston.format.prettyPrint(),
        winston.format.printf(({ level, message, label, timestamp }) => `[${label}] ${timestamp} ${level}: ${message}`),
        winston.format.errors({ stack: true }),
      ),
      transports: [
        new winston.transports.Console({
          //label: moduleName
        })
      ]
    });
  } else logger = externalLogger;
  return logger;
}

function _convertIfJSON(str) {
  var result;
  try {
    result = JSON.parse(str);
  } catch (e) {
    result = str;
  }
  return result;
}

function now(unit) {

  const hrTime = process.hrtime();

  switch (unit) {

    case 'milli':
      return (hrTime[0] * 1000 + hrTime[1] / 1000000);

    case 'micro':
      return (hrTime[0] * 1000000 + hrTime[1] / 1000);

    case 'nano':
      return hrTime[0] * 1000000000 + hrTime[1];

    default:
      return now('nano');
  }

};