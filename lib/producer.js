"use strict";

// This class sends messages without a key to kafka topics

const EventEmitter = require('events');
const Kafka = require('node-rdkafka');
const async = require("async");
const { v4: uuid } = require('uuid');
const Logger = require("./logger.js");
//const sharedFunctions = require("./shared.js");


class KafkaProducer extends EventEmitter {

    static MessageType = Object.freeze({ "AUTO": 1, "JSON": 2, "AVRO": 3, "STRING": 4, "BUFFER": 5, "CUSTOM": 6 })

    #logger = null;
    #producerConfig = null;
    #nodeRDKafkaConfig = null;
    #nodeRDKafkaTopicConfig = null;
    #encoder = null;
    #producer = null;
    #isConnecting = false;
    #isDisconnecting = false;
    #isConnected = false;
    #disconnecting=false;
    #brokerTopics = new Map();
    #inQueue = 0;
    #forcedDisconnecting=false;
    #inErrorShutdown=false;

    #statsMessagesSent = 0;
    #statsBytesSent = 0;
    #statsLastTimestamp = null;
    #statsIntervalTimer = null;
    #nextOpaqueToken = 0;
    #deliveryCallbacks = new Map();
    #statsLatency = 0;


    constructor(producerConfig, logger) {
        super(); //must call super for "this" to be defined.

        this.#logger = new Logger(logger);
        this.#logger.info('Producer initializing');

        this.#producerConfig = {};
        this.#producerConfig.compression = producerConfig.compression || "none";
        this.#producerConfig.clientId = producerConfig.clientId || uuid();
        this.#producerConfig.minProduceLatencyMs = producerConfig.minProduceLatencyMs || 100;
        //this.#producerConfig.queueSize = producerConfig.queueSize || 2000;
        this.#producerConfig.maxMessagesInFlight = producerConfig.maxMessagesInFlight || 2000;
        this.#producerConfig.schemaRegistryUrls = producerConfig.schemaRegistryUrls || null;
        this.#producerConfig.kafkaBrokers = producerConfig.kafkaBrokers || ["localhost:9092"];
        this.#producerConfig.statsIntervalSec = producerConfig.statsIntervalSec || null;
        this.#producerConfig.compressionCodec = producerConfig.compressionCodec || 'none';
        this.#producerConfig.automaticGracefulShudown = producerConfig.automaticGracefulShudown || true;

        if (!Array.isArray(producerConfig.kafkaBrokers)) this.#producerConfig.kafkaBrokers = [producerConfig.kafkaBrokers];
        if (!Array.isArray(producerConfig.schemaRegistryUrls)) this.#producerConfig.schemaRegistryUrl = [producerConfig.schemaRegistryUrl];


        //let batchSize = this.#producerConfig.maxMessagesInFlight / 2;
        //if (batchSize < 1) batchSize = 1;

        //Create consumer and producer configs
        this.#nodeRDKafkaConfig = {
            'client.id': this.#producerConfig.clientId,
            //'message.max.bytes': 15728640,
            'metadata.broker.list': this.#producerConfig.kafkaBrokers,
            'max.in.flight.requests.per.connection': 1,
            'retry.backoff.ms': 10,
            //'compression.codec': producerConfig.compression,
            'message.send.max.retries': 10,
            'socket.keepalive.enable': true,
            'queue.buffering.max.messages': this.#producerConfig.maxMessagesInFlight,
            'linger.ms': this.#producerConfig.minProduceLatencyMs,
            'batch.num.messages': this.#producerConfig.maxMessagesInFlight,
            'dr_cb': true
        };
        this.#nodeRDKafkaTopicConfig = {
            'request.required.acks': 1,
            'compression.codec': this.#producerConfig.compressionCodec
        }

        if (this.#producerConfig.schemaRegistryUrls) {
            this.#logger.info('Producer has schema registry: ' + JSON.stringify(this.#producerConfig.schemaRegistryUrls));
            const AvroCoder = require("./avroCoder");
            this.#encoder = new AvroCoder(this.#producerConfig.schemaRegistryUrls);
        }

        //Setup kafka Producer
        this.#producer = new Kafka.Producer(this.#nodeRDKafkaConfig, this.#nodeRDKafkaTopicConfig);



        const self = this

        this.#producer.on('event.error', function (err) {
            self.#logger.error("EVENT ERROR: " + JSON.stringify(err));
        })


        this.#producer.on('delivery-report', function (err, report) {
            if (err) self.#logger.error("DR: " + JSON.stringify(err));
            else {
                const messageState = self.#deliveryCallbacks.get(report.opaque);


                if (messageState.deliverCallback) messageState.deliverCallback(report.opaque);
                self.#inQueue--;
                self.#statsLatency = self.#statsLatency + ((new Date()) - messageState.submitted);
                self.#statsMessagesSent++;
                self.#statsBytesSent = self.#statsBytesSent + report.size;
                //console.log("DR: " + JSON.stringify(report));
            }
            //else self.#inQueue--;
        });

        this.#producer.on('ready', function (part1, part2) {
            //self.#logger.info("READY: " + JSON.stringify(part1) + "\n" + JSON.stringify(part2));
            //else self.#inQueue--;
        });

        //Do gracefull shutdown on SIGINT, SIGTERM, uncaughtException and unhandledRejection if enabled (automaticGracefulShudown==true)
        if (this.#producerConfig.automaticGracefulShudown) {
            process.on('SIGINT', () => {
                self.#logger.info(`Process ${process.pid} has been interrupted`);
                shutdownConsumer("external signal (SIGINT)");
            });

            process.on('SIGTERM', () => {
                self.#logger.info(`Process ${process.pid} received a SIGTERM signal`);
                shutdownConsumer("external signal (SIGTERM)");
            });

            process.on('uncaughtException', err => {
                self.#logger.error(`Uncaught Exception: ${err.message}`);
                handleExeption("uncaught Exception");
            })

            process.on('unhandledRejection', (reason, promise) => {
                self.#logger.error('Unhandled rejection at ', promise, `reason: ${reason.message}`);
                handleExeption("unhandled Rejection");
            })

            function handleExeption(reason) {
                if (!self.#inErrorShutdown) {
                    self.#inErrorShutdown = true;
                    shutdownConsumer(reason);
                } else {
                    self.#logger.error("Gracefull shutdown failed");
                }
            }

            async function shutdownConsumer(reason) {
                if (!self.#disconnecting && self.#isConnected) {
                    self.#logger.info("Starting gracefull shutdown of producer in response to " + reason);
                    await self.disconnect();
                    self.#forcedDisconnecting = true;
                    self.#inErrorShutdown = false;
                    self.#logger.info("Gracefull shutdown of producer complete");
                }
            }
        }
    }

    async connect() {
        if (!this.#isConnected) {
            this.#isConnecting = true;
            this.#logger.info('Connecting to brokers: ' + JSON.stringify(this.#producerConfig.kafkaBrokers));
            await new Promise((resolve, reject) => {
                this.#producer.connect((err) => {
                    if (err) reject();
                });
                let self = this;
                this.#producer.once('ready', () => {
                    this.#producer.getMetadata({}, (error, rawBrokerTopics) => {
                        rawBrokerTopics.topics.forEach(topic => {
                            this.#brokerTopics.set(topic.name, topic.partitions.length);
                            //this.logger.silly('Recieved valid valid topic from broker: ' + topic.name + " (" + topic.partitions.length + ")");
                        })

                        if (this.#producerConfig.minProduceLatencyMs >= 2) this.#producer.setPollInterval(this.#producerConfig.minProduceLatencyMs / 2);
                        else this.#producer.setPollInterval(1);

                        if (this.#producerConfig.statsIntervalSec) this.#statsIntervalTimer = setInterval(() => this.#doStats(), this.#producerConfig.statsIntervalSec * 1000);

                        this.#isConnected = true;
                        this.#logger.info('Connected producer to brokers: ' + JSON.stringify(this.#producerConfig.kafkaBrokers));
                        this.emit('producer.connected');

                        resolve();
                    });
                });
            });
            this.#isConnecting = false;
        }
    }


    async disconnect() {
        if (!this.#forcedDisconnecting) {
            if (this.#isConnected) {
                this.#isDisconnecting = true;
                this.#logger.info('Disconnecting producer from Kafka broker(s)');
                await new Promise((resolve, reject) => {
                    if (this.#statsIntervalTimer) {
                        clearInterval(this.#statsIntervalTimer);
                        this.#statsIntervalTimer == null;
                    }
                    //??? This section need refactoring
                    async.whilst(
                        (done) => {
                            done(null, this.#inQueue > 0);
                        },
                        (done) => {
                            setTimeout(() => { done(null); }, 10);
                        },
                        (err) => {
                            if (err) reject(err);
                            else {
                                this.#doStats()
                                this.#producer.disconnect((err) => {
                                    if (err) reject();
                                });
                                this.#producer.once('disconnected', () => {
                                    this.#brokerTopics = new Map();
                                    this.#isConnected = false;
                                    this.#forcedDisconnecting=false;
                                    this.#logger.info('Disconnected procucer from Kafka broker(s)');
                                    this.emit('producer.disconnected');
                                    resolve();
                                });
                            }
                        });
                });
                this.#isDisconnecting = false;
            }
        }
    }

    async send(topic, message, key, deliverCallback, messageType, schema) {

        let encodedMessage = null;
        if ((message === null) || (message === "")) encodedMessage = Buffer.from("");
        else if (messageType = KafkaProducer.MessageType.JSON) {
            encodedMessage = Buffer.from(JSON.stringify(message));
        }
        else if (messageType = KafkaProducer.MessageType.STRING) {
            encodedMessage = Buffer.from(message);
        }
        else if (messageType = KafkaProducer.MessageType.BUFFER) {
            encodedMessage = message;
        }
        else throw ("Encodertype not supported yet")

        let encodedKey = null;
        if ((key === null) || (key === "")) encodedKey = Buffer.from("");
        else encodedKey = Buffer.from(key.toString());



        /*let avroEncoded = false;
        if (schema) {
            if (schema === true) message = await this.#encoder.encode(message, topic);
            else message = await this.#encoder.encode(message, topic, schema);
            avroEncoded = true;
        }*/

        let self = this;
        async function _send(topic, message, key, opaqeToken, schema) {
            await new Promise((resolve, reject) => {
                //let sendMessage = Buffer.from(message);
                if (!self.#statsLastTimestamp) self.#statsLastTimestamp = new Date();
                self.#producer.produce(topic, null, encodedMessage, key, null, opaqeToken);
                //console.log(self.#inQueue);
                self.#inQueue++;
                resolve();
            });
        }

        await async.whilst(
            async () => {
                return (this.#inQueue >= (this.#producerConfig.maxMessagesInFlight));
            },
            async () => {
                //this.#producer.poll();
                //Yield while queue is full
                await new Promise((resolve, reject) => {
                    setImmediate(() => {
                        resolve();
                    });
                });
            }
        );

        let messageState = {
            submitted: new Date()
        }
        if (deliverCallback) messageState.deliverCallback = deliverCallback;
        this.#deliveryCallbacks.set(this.#nextOpaqueToken, messageState);

        await _send(topic, encodedMessage, encodedKey, this.#nextOpaqueToken);
        this.#nextOpaqueToken++;
        return this.#nextOpaqueToken - 1;

        //} else {throw("Producer is not connected")};
        //});
    }

    #doStats() {

        const now = new Date();
        const diff = (now - this.#statsLastTimestamp) / 1000;
        const processingSpeed = Math.round(this.#statsMessagesSent / diff);
        const latency = this.#statsLatency / this.#statsMessagesSent;
        const processingVolumeKbs = (this.#statsBytesSent / diff) / 1024;

        this.#logger.info("Sent " + processingSpeed + " msg/s (" + processingVolumeKbs.toFixed(0) + " kb/s) Latency: " + latency.toFixed(2) + " ms  Messages sent: " + this.#statsMessagesSent);
        this.#statsMessagesSent = 0;
        this.#statsBytesSent = 0;
        this.#statsLatency = 0;

        this.#statsLastTimestamp = now;
    }

}

module.exports = KafkaProducer;