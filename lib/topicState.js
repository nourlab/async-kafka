"use strict";

//Package dependencies
const EventEmitter = require('events');

//Project dependencies
const PartitionState = require("./partitionState");
const MessageDecoder = require("./decodeMessage.js");
const Logger = require("./logger.js");

class TopicState extends EventEmitter {

    // Static properties (ENUMS/Types)
    static TopicStatus = Object.freeze({ "UNSUBSCRIBED": 1, "SUBSCRIBED": 2, "ASSIGNED": 3, "CONSUMING": 4, "LOADED": 5 })
    static TopicConsumeMode = Object.freeze({ "EARLIEST": 1, "LAST_COMMIT": 2, "LATEST": 3, "NEXT": 4 });
    static TopicCommitMode = Object.freeze({ "AUTO": 1, "MANUAL": 2, "NONE": 3 });
    static PartitionStatus = PartitionState.PartitionStatus;

    //Private properties
    #partitions = new Map();
    #topicConfig;
    #status = TopicState.TopicStatus.SUBSCRIBED;
    #consumerState = null;
    #logger;

    constructor(topicConfig, consumerState,logger) {
        super();
        if (!topicConfig?.topic) throw ("Constructor need topic in topicConfig to initialize");
        if (!consumerState) throw ("Constructor need consumerState to initialize");
        this.#topicConfig = {
            topic: topicConfig.topic,
            decoderType: topicConfig.decoderType || MessageDecoder.DecoderType.AUTO,
            consumeMode: topicConfig.consumeMode || TopicState.TopicConsumeMode.EARLIEST,
            autoCommit: topicConfig.autoCommit || false,
            keepOffsetOnReassign: topicConfig.keepOffsetOnReassign || true,
            decoder: topicConfig.decoder || null,
            filter: topicConfig.filter || null,
            mapper: topicConfig.mapper || null,
            processor: topicConfig.processor || null,
        };
        this.#consumerState = consumerState;

        this.#logger=new Logger(logger);

        /*this.#consumerState.on("unassigned", () => {
            if (this.isAssigned()) this.#setStatus(TopicState.TopicStatus.SUBSCRIBED);
            else throw ("Trying to unassign unassigned topic");
        })*/

    }

    isAssigned() { return (this.#status == TopicState.TopicStatus.ASSIGNED) || (this.#status == TopicState.TopicStatus.LOADED) };
    isLoaded() { return (this.#status == TopicState.TopicStatus.LOADED) };
    isAutoCommit() { return this.#topicConfig.autoCommit };
    isKeepOffsetOnReassign() { return this.#topicConfig.keepOffsetOnReassign };
    isCommited(){
        let commited=true;
        this.#partitions.forEach((partition=>{
            if(!partition.isCommited()) commited=false;
        }))
        return commited;
    }

    getStatus() { return this.#status };
    getTopic() { return this.#topicConfig.topic };
    getAutoCommit() { return this.#topicConfig.autoCommit };

    getDecoderType() { return this.#topicConfig.decoderType };
    getDecoder() { return this.#topicConfig.decoder };
    getFilter() {
        return this.#topicConfig.filter
    };
    getMapper() { return this.#topicConfig.mapper };
    getProcessor() { return this.#topicConfig.processor };
    getPartitions() { return this.#partitions };
    getPartition(partition) {
        let partitionState = this.#findOrCreatePartition(partition);
        return partitionState;
    }

    getConsumeMode() { return this.#topicConfig.consumeMode };
    //getCommitMode(){ return this.#topicConfig.commitMode };

    /*subscribe() {
        if (this.#status == TopicState.TopicStatus.UNSUBSCRIBED) {
            this.#setStatus(TopicState.TopicStatus.SUBSCRIBED);
            this.emit("subscribed");
        }
        else throw ("Trying to subscribe to topic that is subscribed");
    }

    unsubscribe() {
        if (this.#status == TopicState.TopicStatus.SUBSCRIBED) {
            this.#setStatus(TopicState.TopicStatus.UNSUBSCRIBED);
            this.emit("unsubscribed");
        }
        else throw ("Trying to unsubscribe from topic that has assignments");
    }*/

    assignPartition(partition) {
        const partitionState = this.#findOrCreatePartition(partition);
        partitionState.assign();
        return partitionState;
    }

    /*unassign(partition) {
        const partitionState = this.#findOrCreatePartition(partition);
        if (this.#status != TopicState.TopicStatus.UNSUBSCRIBED) partitionState.unassign();
        else throw ("Trying to assign unsbscribed or assigned partition to topic");
    }

    isSeeking(partition) {
        if (partition != undefined) {
            const partitionObject = this.#partitions.get(partition);
            if (partitionObject) return partitionObject.isSeeking();
            else throw ("Trying to isSeeking on non existing partition");
        } else throw ("isSeeking need partition  properties set");
    };

    setSeeking(partition) {
        if (partition != undefined) {
            const partitionObject = this.#partitions.get(partition);
            if (partitionObject) partitionObject.setSeeking();
            else throw ("Trying to setSeeking on non existing partition");
        } else throw ("setSeeking need partition  properties set");
    };

    getRecievedOffset(partition) {
        if (partition != undefined) {
            const partitionObject = this.#partitions.get(partition);
            if (partitionObject) return partitionObject.getRecievedOffset();
            else throw ("Trying to getRecievedOffset on non existing partition");
        } else throw ("getRecievedOffset need partition  properties set");
    };
    getProcessedOffset(partition) {
        if (partition != undefined) {
            const partitionObject = this.#partitions.get(partition);
            if (partitionObject) return partitionObject.getProcessedOffset();
            else throw ("Trying to getProcessedOffset on non existing partition");
        } else throw ("getProcessedOffset need partition  properties set");
    };
    getCommitedOffset(partition) {
        if (partition != undefined) {
            const partitionObject = this.#partitions.get(partition);
            if (partitionObject) return partitionObject.getCommitedOffset();
            else throw ("Trying to getCommitedOffset on non existing partition");
        } else throw ("getCommitedOffset need partition  properties set");
    };

    setRecievedOffset(partition, recievedOffset) {
        if ((partition != undefined) && recievedOffset) {
            const partitionObject = this.#partitions.get(partition);;
            if (partitionObject) partitionObject.setRecievedOffset(recievedOffset);
            else throw ("Trying to setRecievedOffset on non existing partition");
        } else throw ("setRecievedOffset need partition and recievedOffset properties set");
    }

    setProcessedOffset(partition, processedOffset) {
        if ((partition != undefined) && processedOffset) {
            const partitionObject = this.#partitions.get(partition);
            if (partitionObject) partitionObject.setProcessedOffset(processedOffset);
            else throw ("Trying to setProcessedOffset on non existing partition");
        } else throw ("setProcessedOffset need partition and processedOffset properties set");
    }

    setCommitedOffset(partition, commitedOffset) {
        if ((partition != undefined) && commitedOffset) {
            const partitionObject = this.#partitions.get(partition);
            if (partitionObject) partitionObject.setCommitedOffset(commitedOffset);
            else throw ("Trying to setCommitedOffset on non existing partition");
        } else throw ("setCommitedOffset need partition and commitedOffset properties set");
    }

    setPartitionLoaded(partition) {
        if (partition != undefined) {
            const partitionObject = this.#partitions.get(partition);
            if (partitionObject) {
                partitionObject.setPartitionLoaded();
                //if (this.countAssignedPartitions == this.countLoadedPartitions()) this.isLoaded = true;
            }
            else throw ("Trying to set loaded on non existing partition");
        } else throw ("setPartitionLoaded need partition property set");
    }*/

    #setStatus(status) {
        function propName(status) {
            for (var i in TopicState.TopicStatus) {
                if (TopicState.TopicStatus[i] == status) {
                    return i;
                }
            }
            return false;
        }
        if (status != this.#status) {
            this.#logger.log("TOP " + propName(status) + ": " + this.#topicConfig.topic);
            this.#status = status;
            //this.emit("statusChange", status);
        }
    }

    #findOrCreatePartition(partition) {
        let partitionObject = this.#partitions.get(partition);
        if (!partitionObject) {
            partitionObject = new PartitionState(this.#topicConfig.topic, partition, this.#topicConfig.keepOffsetOnReassign, this.#consumerState, this,this.#logger.getLogger());
            partitionObject.on("unassigned", () => { this.#processPartitionStatusEvent("unassigned") });
            partitionObject.on("assigned", () => { this.#processPartitionStatusEvent("assigned") });
            //partitionObject.on("consuming", (status) => { this.#processPartitionStatusEvent("consuming") });
            partitionObject.on("loaded", (status) => { this.#processPartitionStatusEvent("loaded") });
            partitionObject.on("seeking", (status) => { this.#processPartitionStatusEvent("seeking") });
            this.#partitions.set(partition, partitionObject);
        }
        return partitionObject;
    }

    #processPartitionStatusEvent(event) {
        if (event == "assigned") {
            if (!this.isAssigned()) {
                this.#setStatus(TopicState.TopicStatus.ASSIGNED);
            }
            this.emit("assigned");
        } else if (event == "unassigned") {
            let assigned = false;
            this.#partitions.forEach((partition) => {
                if (partition.isAssigned()) assigned = true;
            });
            if (!assigned && this.isAssigned()) {
                this.#setStatus(TopicState.TopicStatus.SUBSCRIBED);
                this.emit("unassigned");
            }
            else if (!this.isAssigned()) throw ("Partition send Unassign event to unassigned topic");
        } else if (event == "seeking") {
            if (this.isLoaded()) {
                this.#setStatus(TopicState.TopicStatus.ASSIGNED);
                this.emit("seeking");
            }
            else if (!this.isAssigned()) throw ("Partition send seeking event to unassigned topic");
        } else if (event == "loaded") {
            let loaded = true;
            this.#partitions.forEach((partition) => {
                if (!partition.isLoaded()) loaded = false;
            });
            if (loaded && !this.isLoaded()) {
                this.#setStatus(TopicState.TopicStatus.LOADED);
                this.emit("loaded");
            }
            else if (this.isLoaded()) throw ("Partition send loaded event to loaded topic");
        }
    }

}

module.exports = TopicState;