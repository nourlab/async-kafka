"use strict";

class Logger{

    #logger;

    constructor(logger){
        if(logger) this.#logger=logger;
        else this.#logger=logger;
    }

    getLogger() {return this.#logger}

    log(...args){
        if(this.#logger?.log) this.#logger.log(...args);
    }

    error(...args){
        if(this.#logger?.error) this.#logger.error(...args);
    }

    warn(...args){
        if(this.#logger?.warn) this.#logger.warn(...args);
    }

    debug(...args){
        if(this.#logger?.debug) this.#logger.debug(...args);
    }

    assert(...args){
        if(this.#logger?.assert) this.#logger.assert(...args);
    }

    info(...args){
        if(this.#logger?.info) this.#logger.info(...args);
    }

    dir(...args){
        if(this.#logger?.dir) this.#logger.dir(...args);
    }
}

module.exports = Logger;