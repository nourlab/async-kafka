class AvroCoder {
    constructor(schemaRegisterUrls) {
        //Set schema registers
        const clone = require("clone");
        if (!Array.isArray(schemaRegisterUrls)) this.schemaRegisterUrls = [clone(schemaRegisterUrls)];
        else this.schemaRegisterUrls = clone(schemaRegisterUrls);
        this.currentSchemaRegisterId = 0;
        this.failures = 0;
        this.registry = require('avro-schema-registry')(this.schemaRegisterUrls[this.currentSchemaRegisterId]);
    }

    async encode(message, topic, schema) {
        let result;
        try {
            if (topic && schema) {
                result = await this.registry.encodeMessage(topic, schema, message);
            } else if (topic) {
                result = await this.registry.encodeMessageByTopicName(topic, message);
            } else throw new Error("No schema or topic");
        } catch (err) {
            this.failures++;
            if (this.failures >= this.schemaRegisterUrls.length) throw (new Error("No contact with schema register(s): " + JSON.stringify(this.schemaRegisterUrls)));
            else {
                _AvroCoder_setNextSchemaRegister(this);
                result = await this.encode(message, topic, schema);
            }
        }

        return result;
    }

    async decode(message) {

        let result;
        try {
            result = await this.registry.decode(message);
        } catch (err) {
            this.failures++;
            if (this.failures >= this.schemaRegisterUrls.length) throw(new Error("No contact with schema register(s): " + JSON.stringify(this.schemaRegisterUrls)));
            else {
                _AvroCoder_setNextSchemaRegister(this);
                result = await this.decode(message);
            }
        }
        return result;
    }


    isAvro(message) {
        if (message.readUInt8(0) == 0) return true;
        else return false;
    }

    async getTopicSchema(topic) {
        let result;
        try {
            result = await this.registry.getSchemaByTopicName(topic);
        } catch (err) {
            this.failures++;
            if (this.failures >= this.schemaRegisterUrls.length) throw(new Error("No contact with schema register(s): " + JSON.stringify(this.schemaRegisterUrls)));
            else {
                _AvroCoder_setNextSchemaRegister(this);
                result = await this.getTopicSchema(topic);
            }
        }
        return result;
    }
}

function _AvroCoder_setNextSchemaRegister(context) {
    context.currentSchemaRegisterId++;
    if (context.currentSchemaRegisterId >= context.schemaRegisterUrls.length) context.currentSchemaRegisterId = 0;
    context.registry = require('avro-schema-registry')(context.schemaRegisterUrls[currentSchemaRegisterId]);
}

module.exports = AvroCoder;