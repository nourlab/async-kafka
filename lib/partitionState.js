"use strict";

//Package dependencies
const EventEmitter = require('events');
const Logger = require("./logger.js");

class PartitionState extends EventEmitter {

    //Static properties
    static PartitionStatus = Object.freeze({ "UNASSIGNED": 1, "ASSIGNED": 2, "CONSUMING": 3, "LOADED": 4, "SEEKING": 5 });
    static SeekMode = Object.freeze({ "EARLIEST": 1, "COMMITED": 2, "FORWARD": 3, "BACKWARD": 4, "LATEST": 5, "NEXT": 6, "REASSIGN": 7, "NONE": 8 });

    //Private properties
    #status = PartitionState.PartitionStatus.UNASSIGNED;
    #topic;
    #partition;
    #keepOffsetOnReassign;
    #seekMode = PartitionState.SeekMode.NONE;
    #seekOffset = null;
    #eofOffset = null;
    #firstRecievedOffset = null;
    #lastRecievedOffset = null;
    #firstProcessedOffset = null;
    #lastProcessedOffset = null;
    #lastCommitedOffset = null;
    #consumerState = null;
    #topicState = null;
    #logger;

    constructor(topic, partition, keepOffsetOnReassign, consumerState, topicState,logger) {
        super();
        if (!topic || (!partition && (partition !== 0))) throw ("Constructor need topic and partition to initialize");
        this.#topic = topic;
        this.#partition = partition;
        this.#keepOffsetOnReassign = keepOffsetOnReassign || true;
        //this.#isAutoCommit = topicState.isAutoCommit() || false;
        this.#consumerState = consumerState;
        this.#topicState = topicState;

        this.#logger=new Logger(logger);

        /*this.#consumerState.on("unassigned", () => {
            this.unassign();
        });*/
    }


    isAssigned() { return (this.#status != PartitionState.PartitionStatus.UNASSIGNED) };
    isLoaded() { return (this.#status == PartitionState.PartitionStatus.LOADED) };
    isSeeking() { return (this.#status == PartitionState.PartitionStatus.SEEKING) };
    isKeepOffsetOnReassign() { return this.#keepOffsetOnReassign };
    isCommited() { return (this.#lastProcessedOffset === this.#lastCommitedOffset) };

    getStatus() { return this.#status };
    getTopic() { return this.#topic };
    getPartition() { return this.#partition };

    assign() {
        if (!this.isAssigned()) {
            if (this.#seekMode == PartitionState.SeekMode.REASSIGN) {
                this.#lastRecievedOffset = this.#seekOffset - 1;
            }
            this.#setStatus(PartitionState.PartitionStatus.ASSIGNED);
            this.emit("assigned");
        }
        else throw ("Trying to assign partition that is assigned");
    }

    unassign() {
        //this.#logger.log(this.#status,this.isAssigned())
        if (this.isAssigned()) {
            if (!this.#topicState.isAutoCommit() && this.isKeepOffsetOnReassign()) {
                this.#seekOffset = this.#lastProcessedOffset + 1;
                this.#seekMode = PartitionState.SeekMode.REASSIGN;
            }
            this.#eofOffset = null;
            this.#firstRecievedOffset = null;
            this.#lastRecievedOffset = null;
            this.#lastProcessedOffset = null;
            this.#lastCommitedOffset = null;
            this.#setStatus(PartitionState.PartitionStatus.UNASSIGNED);
            this.emit("unassigned");

        } else throw ("Trying to unassign partition that is unassigned");
    }

    recievedEof(eofOffset) {
        //if (this.#status == PartitionState.PartitionStatus.UNASSIGNED) throw ("Trying to set loaded on partition that is unnasigned");
        if (((eofOffset - 1) == this.#lastRecievedOffset) && !this.#firstRecievedOffset) {
            this.#setStatus(PartitionState.PartitionStatus.LOADED);
            this.#eofOffset = null;
            this.emit("loaded");
        }
        else if ((!this.#eofOffset) && !this.isLoaded()) this.#eofOffset = eofOffset - 1;
        /*if (this.isAssigned() && !this.isLoaded()) {
            this.#setStatus(PartitionState.PartitionStatus.LOADED);
            if (this.isSeeking()) {
                this.#seekMode = PartitionState.SeekMode.NONE;
                this.#seekOffset = null;
            }
            this.emit("loaded");
        } else if (!this.isAssigned()) throw ("Trying to set unassigned or loaded partition to loaded");*/
    }

    seek(seekMode, offset) {
        this.#seekMode = seekMode;
        //if ((offset !== null) && (offset !== undefined)) {
        this.#seekOffset = offset;
        this.#lastRecievedOffset = null;
        this.#lastProcessedOffset = null;
        this.#firstRecievedOffset = null;
        this.#lastCommitedOffset = null;
        if (!this.isSeeking()) {
            this.#setStatus(PartitionState.PartitionStatus.SEEKING);
            this.emit("seeking");

        }

        //}
    };


    getSeekMode() { return this.#seekMode }
    getSeekOffset() { return this.#seekOffset }
    getFirstRecievedOffset() { return this.#firstRecievedOffset; }
    getRecievedOffset() { return this.#lastRecievedOffset; }
    getProcessedOffset() { return this.#lastProcessedOffset; }
    getCommitedOffset() { return this.#lastCommitedOffset; }

    setRecievedOffset(recievedOffset) {
        if (recievedOffset || (recievedOffset === 0)) {
            if (this.isAssigned()) {
                //if (!this.#lastCommitedOffset) this.#lastCommitedOffset = recievedOffset - 1;
                if ((recievedOffset > this.#lastRecievedOffset) || (this.#lastRecievedOffset === null) || (!this.#firstRecievedOffset)) {
                    if (!this.#firstRecievedOffset) {
                        //this.#logger.log(recievedOffset, this.#lastRecievedOffset, this.#firstRecievedOffset)
                        this.#firstRecievedOffset = recievedOffset;
                    }
                    this.#lastRecievedOffset = recievedOffset;
                    if (this.isSeeking()) {
                        this.#setStatus(PartitionState.PartitionStatus.ASSIGNED);
                        this.#seekMode = PartitionState.SeekMode.NONE;
                        this.#seekOffset = null;
                    }
                } else {
                    //this.#logger.log("MISMATCH " + this.#topic + "/" + this.#partition);
                    //this.#logger.log(recievedOffset, this.#lastRecievedOffset, this.#firstRecievedOffset)
                    throw ("Function setRecievedOffset need recievedOffset to be larger than previous recievedOffset");
                }
            }
        } else throw ("Function setRecievedOffset need recievedOffset parameter set");
    }

    setProcessedOffset(prosessedOffset) {
        if (prosessedOffset || (prosessedOffset === 0)) {
            if (this.isAssigned() && !this.isSeeking()) {
                if (!this.#firstProcessedOffset) this.#firstProcessedOffset = prosessedOffset;
                if ((prosessedOffset > this.#lastProcessedOffset) || (this.#lastProcessedOffset === null)) {
                    this.#lastProcessedOffset = prosessedOffset;
                    if ((this.#eofOffset) && (this.#eofOffset <= prosessedOffset)) {
                        this.#setStatus(PartitionState.PartitionStatus.LOADED);
                        this.#eofOffset = null;
                        this.emit("loaded");
                    }
                } else throw ("Function setProcessedOffset need prosessedOffset to be larger than previous prosessedOffset");
            } else throw ("Trying to set processed offset on partition that is not assigned or is in seek mode");
        } else throw ("Function setProcessedOffset need prosessedOffset parameter set");
    }

    //???????? Need this?
    setCommitedOffset(commitedOffset) {
        if (commitedOffset || (commitedOffset === 0)) {
            if ((commitedOffset >= this.#lastCommitedOffset) || (this.#lastCommitedOffset === null)) this.#lastCommitedOffset = commitedOffset;
            else throw ("Function setCommitedOffset need commitedOffset to be larger than previous commitedOffset");
        } else throw ("Function setCommitedOffset need commitedOffset parameter set");
    }

    //Partition is set as loaded if consumer recieve eof event for partition

    /*setSeekMode(seekMode) {
            if (seekMode != PartitionState.SeekMode.NONE) {
                //this.#setStatus(PartitionState.PartitionStatus.SEEKING);
                this.#seekMode = seekMode;
            } else throw ("You cannot manualy clear seekmode with NONE");
        };*/
    #setStatus(status) {
        function propName(status) {
            for (var i in PartitionState.PartitionStatus) {
                if (PartitionState.PartitionStatus[i] == status) {
                    return i;
                }
            }
            return false;
        }
        if (status != this.#status) {
            this.#logger.log("PAR " + propName(status) + ": " + this.#topic + "/" + this.#partition);
            this.#status = status;
        }
    }


}

module.exports = PartitionState;